import React from 'react'
import { graphql } from 'gatsby'
import { RichText } from 'prismic-reactjs'
import { withPreview } from 'gatsby-source-prismic'
import Layout from '../components/layouts'
import BlogPosts from '../components/BlogPosts'

export const query = graphql`
  query MyQuery {
    allPrismicBlog {
      edges {
        node {
          data {
            category
            title {
              text
            }
            seo_description {
              text
            }
            seo_title {
              text
            }
            featured_image {
              url
            }
      
            blog_content {
      
              text
      
           
            }
          }
          
          url
          uid
  
      
          lang
          id
          href
        }
      }
    }
  }
`

// Using the queried Blog Home document data, we render the top section
// const BlogHomeHead = ({ home }) => {
//   const avatar = { backgroundImage: `url(${home.image.url})` }
//   return (
//     <div className="home-header container" data-wio-id={home.id}>
//       <div className="blog-avatar" style={avatar} />
//       <h1>{RichText.asText(home.headline)}</h1>
//       <p className="blog-description">{RichText.asText(home.description)}</p>
//     </div>
//   )
// }

export const Homepage = ({ data }) => {
  if (!data) return null
  // Define the Blog Home & Blog Post content returned from Prismic
  const posts = data.allPrismicBlog.edges

  return (
    <Layout>
      {/* <BlogHomeHead home={home} /> */}
      <BlogPosts posts={posts} />
    </Layout>
  )
}

export default withPreview(Homepage)
