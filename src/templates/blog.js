import React from 'react'
import { graphql, Link } from 'gatsby'
import { RichText } from 'prismic-reactjs'
import { withPreview } from 'gatsby-source-prismic'
import Layout from '../components/layouts'


export const query = graphql`
query BlogPostQuery($uid: String)  {
  prismicBlog(uid: { eq: $uid }) {
    uid
    lang
    data {
      category
      title {
        text
      }
      seo_description {
        text
      }
      seo_title {
        text
      }
      featured_image {
        url
      }

      blog_content{
        html
        text
    }
    }
  }
}

`
// Sort and display the different slice options

// Display the title, date, and content of the Post
const PostBody = ({ blogPost }) => {
  console.log(blogPost.title.text)
  return (
    
    <div>
      	<div className="post-wrap">
			<div className="post-above">
				<div className="post-first">
        <h1>{blogPost.title.text}</h1>
				</div>
   
        <div className="above-image">
                    <img src={blogPost.featured_image.url} />
                </div>
			</div>
		<div className="post-content">
    <div dangerouslySetInnerHTML={{ __html: blogPost.blog_content.html }}/>
		</div>
	</div>
	</div>
  )
}

export const Post = ({ data }) => {
  if (!data) return null
  // Define the Post content returned from Prismic
  const post = data.prismicBlog.data

  return (
    <Layout>
      <PostBody blogPost={post} />
    </Layout>
  )
}

export default withPreview(Post)
